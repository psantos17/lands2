﻿using System.Windows.Input;

namespace Lands.ViewModels
{
    public class LoginViewModel
    {
        #region Constructors
        public LoginViewModel()
        {
            this.IsRemember = true;
            this.IsRunning = false;
        }

        #endregion

        #region Properties

        public string Email
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public bool IsRemember
        {
            get;
            set;
        }

        public bool IsRunning
        {
            get;
            set;
        }

        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            get;
            set;
        }

        #endregion
    }
}
